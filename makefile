install: pcma

clean:
	rm *.o

OBJECTS = interface.o sequence.o showpair.o malign.o \
  	util.o trees.o gcgcheck.o prfalign.o pairalign.o \
  	calcgapcoeff.o calcprf1.o calcprf2.o calctree.o \
        readmat.o alnscore.o random.o alcomp2.o lib_generation.o \
	lsim1.o lib_extension.o prfalignabs.o prfalign1.o prfalign2.o

HEADERS = general.h pcma.h

CC	= gcc
CFLAGS  = -c -O
LFLAGS	= -O -lm -g

pcma : $(OBJECTS) amenu.o pcma.o
	$(CC) -o $@ $(OBJECTS) amenu.o pcma.o $(LFLAGS)

interface.o : interface.c $(HEADERS) param.h
	$(CC) $(CFLAGS) $*.c

amenu.o : amenu.c $(HEADERS) param.h
	$(CC) $(CFLAGS) $*.c

readmat.o : readmat.c $(HEADERS) matrices.h
	$(CC) $(CFLAGS) $*.c

trees.o : trees.c $(HEADERS) dayhoff.h
	$(CC) $(CFLAGS) $*.c

lib_generation.o : lib_generation.c $(HEADERS)
	$(CC) $(CFLAGS) $*.c

lib_extension.o : lib_extension.c $(HEADERS)
	$(CC) $(CFLAGS) $*.c

malign.o : malign.c $(HEADERS)
	$(CC) $(CFLAGS) $*.c

calctree.o : calctree.c $(HEADERS)
	$(CC) $(CFLAGS) $*.c

lsim1.o : lsim1.c $(HEADERS) 
	$(CC) $(CFLAGS) $*.c 

prfalignabs.o : prfalignabs.c $(HEADERS)
	$(CC) $(CFLAGS) $*.c

prfalign1.o: prfalign1.c $(HEADERS)
	$(CC) $(CFLAGS) $*.c

prfalign2.o: prfalign2.c $(HEADERS)
	$(CC) $(CFLAGS) $*.c

.c.o :
	$(CC) $(CFLAGS) $?


