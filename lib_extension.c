#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "pcma.h"
/*#include "new.h"*/

extern SN ****glib;
extern sint     *seqlen_array;
extern char    **seq_array;
extern sint        ngroups;
extern streeptr *groupptr;

#define min(x,y) ((x)<=(y) ? (x) : (y))

int HIGHEST_SCORE = 1000;  /* do normalization: make the highest score to be 1000 */

void lib_extension()
{

	sint i, j, k, l;
	sint ni, nj, nk;
	SN *ndi, *ndk, *ndj, *ndj_b, *nd;
	int newstruct = 0;
	int Max_score = 0;

	/* lib extension: going over all sequence triplets, additional scores stored in sae */
	for(i=0;i<ngroups;i++) {
		for(j=i+1;j<ngroups;j++) {
			for(k=0;k<ngroups;k++) {
				if(k==i) continue;
				if(k==j) continue;

				/*fprintf(stdout, "size: %d\n", sizeof(SN) );*/
				for(ni=1;ni<=groupptr[i]->seqlength;ni++) {
					/*fprintf(stdout, "i %d j %d k %d ni %d \n", i, j, k, ni);*/
					ndi = glib[i][k][ni]; /* fprintf(stdout, "newstruct: %d\n", newstruct);fflush(stdout);*/
					while(ndi!=NULL) {
						if(ndi->sbe==0) {ndi=ndi->next; continue;}
						nk = glib[i][k][ni]->ind;
						ndk = glib[k][j][nk];
						while(ndk!=NULL) {
							nj = ndk->ind;
							if(ndk->sbe==0) {ndk=ndk->next;continue;}
							fflush(stdout);

							if(glib[i][j][ni]==NULL) {
								glib[i][j][ni] = SNavail(); newstruct++;
								glib[i][j][ni]->ind = nj;
								glib[i][j][ni]->sae += min(ndi->sbe, ndk->sbe);
							}
							else {
								ndj = glib[i][j][ni];
								ndj_b = ndj;
								while(ndj!=NULL) {
									if(ndj->ind == nj) {
										ndj->sae += min(ndi->sbe, ndk->sbe);
										break;
									}
									else {ndj_b = ndj; ndj = ndj->next;}
								}
								if(!ndj) {
									ndj_b->next = SNavail();newstruct++;
									ndj = ndj_b->next;
									ndj->ind = nj;
									ndj->sae += min(ndi->sbe, ndk->sbe);
								}
							}

							/*if(glib[j][i][nj]==NULL) {
								glib[j][i][nj] = SNavail();
								glib[j][i][nj]->ind = ni;
								glib[j][i][nj]->sae+=min(ndi->sbe, ndk->sbe);
							}
							else {
								ndj = glib[j][i][nj];
								ndj_b = ndj;
								while(ndj!=NULL) {
									if(ndj->ind == ni) {
										ndj->sae += min(ndi->sbe, ndk->sbe);
										break;
									}
									else {ndj_b = ndj; ndj = ndj->next; }
								}
								if(!ndj) {
									ndj_b->next = SNavail();
									ndj = ndj_b->next;
									ndj->ind = nj;
									ndj->sae += min(ndi->sbe, ndk->sbe);
								}
							}
							*/

							ndk = ndk->next;
						}
						ndi = ndi->next;
					}
				}
			}
		}
	}

	/* adding sbe to sae */
	for(i=0;i<ngroups;i++) {
		for(j=i+1;j<ngroups;j++) {
			/*fprintf(stdout, "# %d %d\n", i, j);*/
			for(ni=1;ni<=groupptr[i]->seqlength;ni++) {
				nd = glib[i][j][ni];
				while(nd) {
					nd->sae += nd->sbe;
					/* record the largest score to do normalization: make the highest score to be 1000 */
					if(nd->sae > Max_score) {
						Max_score = nd->sae;
					}
					/*nd->sae = nd->sae/100;
					fprintf(stdout, "%d %d %d\n", ni, nd->ind, nd->sae);*/
					nd = nd->next;
				}
			}
		}
	}

	/* do normalization */
	for(i=0;i<ngroups;i++) {
		for(j=i+1;j<ngroups;j++) {
			for(ni=1;ni<=groupptr[i]->seqlength;ni++) {
				nd = glib[i][j][ni];
				while(nd) {
					nd->sae = nd->sae*HIGHEST_SCORE/Max_score;
					nd = nd->next;
				}
			}
		}
	}

}
