/* new structures and functions introduced by  Jimin Pei */

/* node for a tree containing the sequences */
typedef struct snode {
 	struct snode *left;
	struct snode *right;
	struct snode *parent;
	float dist;
	sint leaf;
	int order;
	char **name;
	int **seq;
	int seqnum;
	int seqlength;
	int **abstractseq;
	short abseqnum;
	int abseqlength;
} sstree, *streeptr;

/* node for a linked list of scores */
typedef struct scorenode {
	struct scorenode *next;
	short sbe; /* score before extension */
	short ind; /* index of the second sequence */
	int  sae; /* score after extension */
} SN;


/* structure of element of an alignment */
/*typedef struct AlignmentElement {
	int gid;
	int pid;
} AE;
*/

/* function prototypes */
/* lib_generation.c */
SN * SNavail();
void lib_generation();
void AddSbe(SN *node, int indi, int s);
void printLib(int gi, int gj);


/* calctree.c */
void assign_node(streeptr p, sint *aligned);
double average_group_identity(sint *group);

/* al2comp2.c */
void prfprfmatrix(int **align1, int **align2, int alnlength1, int alnlength2, int nali1, int nali2, double **prfprfmat);

/*lsim1.c */
void SIM(int M,int N,int K,int **V,int Q,int R,int nseq,int gi, int gj);

/*lib_extension.c */
void lib_extension();

/*prfalign1.c */
lint prfalign1(sint *group, sint gi, sint gj);
