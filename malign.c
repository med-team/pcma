#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "pcma.h"
/*#include "new.h"*/

/*
 *       Prototypes
 */

/*
 *       Global Variables
 */

extern double  **tmat;
extern Boolean no_weights;
extern sint     debug;
extern sint     max_aa;
extern sint     nseqs;
extern sint     profile1_nseqs;
extern sint     nsets;
extern sint     **sets;
extern sint     divergence_cutoff;
extern sint     *seq_weight;
extern sint     output_order, *output_index;
extern Boolean distance_tree;
extern char    seqname[];
extern char		**names;
extern sint     *seqlen_array;
extern char    **seq_array;
extern sint	   ngroups;
extern streeptr *groupptr;
extern char *amino_acid_codes;
extern sint gap_pos1, gap_pos2;

/* JP */
extern sint res_index(char *,char);
extern void assign_node(streeptr p, sint *aligned);
extern streeptr *grp_ancestor;
extern sint ngroups;
extern sint ave_grp_id;
SN **** glib;
extern sint KK;
extern sint cosmetic_penalty;
extern sint seqFormat;

/* JP: for_align_list */
extern int *seqnumlist;
extern int filecount;
extern int nseqs_all;
extern int *seqlen_array_all;
extern char **seq_array_all; /* for all the sequences */
extern char **names_all;
extern int max_names;

sint malign(sint istart,char *phylip_name) /* full progressive alignment*/
{
   static 	sint *aligned;
   static 	sint *group;
   static 	sint ix;

   sint 	*maxid, max, sum;
   sint		*tree_weight;
   sint 		i,j,k,l,set,iseq=0;
   sint 		status,entries;
   lint		score = 0;

   /* JP */
   sint **tmpseq_array;
   char **tmpnames;

   streeptr nd;

   extern char * am;

   info("Start making multiple sequence alignment\n");

/* get the phylogenetic tree from *.ph */   /************* step 1 ***************/

   if (nseqs >= 2)
     {
       status = read_tree(phylip_name, (sint)0, nseqs);
       if (status == 0) return((sint)0);
     }

/* calculate sequence weights according to branch lengths of the tree -
   weights in global variable seq_weight normalised to sum to 100 */   /************* step 2 ***************/

   calc_seq_weights((sint)0, nseqs, seq_weight);

/* recalculate tmat matrix as percent similarity matrix */ /************* step 3 ***************/

   status = calc_similarities(nseqs);
   if (status == 0) return((sint)0);

/* for each sequence, find the most closely related sequence */

   maxid = (sint *)ckalloc( (nseqs+1) * sizeof (sint));
   for (i=1;i<=nseqs;i++)
     {
         maxid[i] = -1;
         for (j=1;j<=nseqs;j++)
           if (j!=i && maxid[i] < tmat[i][j]) maxid[i] = tmat[i][j];
     }

/* JP */
  if(debug>1)fprintf(stdout, "ave_grp_id: %d; KK: %d cosmetic: %d\n", ave_grp_id, KK, cosmetic_penalty);fflush(stdout);

/* group the sequences according to their relative divergence */

   if (istart == 0)
     {
        sets = (sint **) ckalloc( (nseqs+1) * sizeof (sint *) );
        for(i=0;i<=nseqs;i++)
           sets[i] = (sint *)ckalloc( (nseqs+1) * sizeof (sint) );

        create_sets((sint)0,nseqs); /************* step 4 ***************/
	/*JP */
        /*info("There are %d groups\n",(pint)nsets);*/
	if(seqFormat!=CLUSTALIST) info("There are %d sequences\n\n", nseqs);

/* clear the memory used for the phylogenetic tree */

        if (nseqs >= 2)
             clear_tree(NULL);

/* start the multiple alignments.........  */

	/* JP */
        /*info("Aligning...");*/

/* first pass, align closely related sequences first.... */

        ix = 0;
        aligned = (sint *)ckalloc( (nseqs+1) * sizeof (sint) );
        for (i=0;i<=nseqs;i++) aligned[i] = 0;

	    /* JP: forming the groups ngroups */
	    ngroups = 0; score = 0;
	    groupptr = ckalloc(nseqs*sizeof(streeptr *));  /************* step 5 ***************/

	/* set ouput order */    /************* step 6 ***************/
	if(debug>1) fprintf(stdout, "OUTPUT_ORDER: %d\n", output_order);
        for(set=1;set<=nsets;++set)
         {
          entries=0;
          for (i=1;i<=nseqs;i++)
            {
               if ((sets[set][i] != 0) && (maxid[i] >= divergence_cutoff))
                 {
                    entries++;
                    if  (aligned[i] == 0)
                       {
                          if (output_order==INPUT)
                            {
                              ++ix;
                              output_index[i] = i;
                            }
                          else output_index[++ix] = i;
                          aligned[i] = 1;
                       }
                 }
            }

            /*fprintf(stdout, "entries: %d\n", entries);*/
	  /* JP: conditionally align the sequences to form groups */
          /*if(entries > 0) {score = prfalign(sets[set], aligned);
          else score=0.0;*/

	  /* JP: for_alignlist_set the average group identity threshold to be larger than 100 so that no sequences are merged */
	  if(seqFormat==CLUSTALIST) {ave_grp_id = 101;}

	  /* align highly similar sequences by ClustalW */    /************* step 7 ***************/
          if(entries > 0) {
			  /*fprintf(stdout,"set %d %d*********\n", set, grp_ancestor[set]->left->seq[1][1]);*/
			  /*if(grp_ancestor[set]->right->seq) fprintf(stdout, "--------\n");*/
			  if(grp_ancestor[set]->left->seq && grp_ancestor[set]->right->seq) {
				  if(debug> 1) fprintf(stdout,"average score: %5.3f \n", average_group_identity(sets[set]));
				  if(average_group_identity(sets[set])> ave_grp_id) {
					  if(debug>1)fprintf(stdout,"%5.3f *********\n", average_group_identity(sets[set]));
					  score = prfalign(sets[set], aligned);
					  if(debug>1)fprintf(stdout, "======\n");
					  assign_node(grp_ancestor[set], sets[set]);
				  }
				  else {
					  groupptr[ngroups]=grp_ancestor[set]->left;
					  ngroups++;
					  groupptr[ngroups]=grp_ancestor[set]->right;
					  ngroups++;
			  	  }
		  	  }

		  	  if(grp_ancestor[set]->left->seq && (!grp_ancestor[set]->right->seq) ) {
				  /*fprintf(stdout, "left name: %s\n",grp_ancestor[set]->left->name[1]);*/
				  groupptr[ngroups]=grp_ancestor[set]->left;
				  ngroups++;
			  }

			  if( (!grp_ancestor[set]->left->seq) && (grp_ancestor[set]->right->seq) ) {
				  			  /*fprintf(stdout, "right name: %s\n",grp_ancestor[set]->right->name[1]);*/
			  				  groupptr[ngroups]=grp_ancestor[set]->right;
			  				  ngroups++;
			  }

			  /*if( (!grp_ancestor[set]->left->seq) && (!grp_ancestor[set]->right->seq) ) {
			  				  groupptr[ngroups]=grp_ancestor[set]->left;
			  				  ngroups++;
			  }*/
		  }


	  /* negative score means fatal error... exit now!  */
          /*if (score < 0)
             {
                return(-1);
             }
	  */
          /*if ((entries > 0) && (score > 0))
             info("Group %d: Sequences:%4d      Score:%d\n",
             (pint)set,(pint)entries,(pint)score);
          else
             info("Group %d:                     Delayed",
             (pint)set); */

          }

	/* JP: set still useful */
        /*for (i=0;i<=nseqs;i++)
          sets[i]=ckfree((void *)sets[i]);
        sets=ckfree(sets);
	*/

        /* JP: print out the pre-aligned groups  */
	if(seqFormat!=CLUSTALIST) fprintf(stdout, "Average percentage group identity threshold is %d\n\n", ave_grp_id);
        fprintf(stdout, "There are %d pre-aligned groups \n\n", ngroups);
    if(debug>1) {
        for(i=0;i<ngroups;i++) {
			fprintf(stdout, "group %d\t", i);
			fprintf(stdout, "alignment length: %d\n", groupptr[i]->seqlength);
			for(j=1;j<=groupptr[i]->seqnum;j++) {
				fprintf(stdout, "%s\t", groupptr[i]->name[j]);
				for(k=1;k<=groupptr[i]->seqlength;k++) {
					fprintf(stdout, "%c", am[groupptr[i]->seq[j][k]]);
				}
				fprintf(stdout, "\n");
			}
			fprintf(stdout,"\n");
		}
    }

     }
   else
     {
/* clear the memory used for the phylogenetic tree */

        if (nseqs >= 2)
             clear_tree(NULL);

        aligned = (sint *)ckalloc( (nseqs+1) * sizeof (sint) );
        ix = 0;
        for (i=1;i<=istart+1;i++)
         {
           aligned[i] = 1;
           ++ix;
           output_index[i] = i;
         }
        for (i=istart+2;i<=nseqs;i++) aligned[i] = 0;
     }

        /* JP: assigning abstract sequences */   /************* step 8 ***************/
        for(i=0;i<ngroups;i++) {
           j = groupptr[i]->seqlength;
           groupptr[i]->abseqnum = 1;
	   groupptr[i]->abseqlength = j;
           groupptr[i]->abstractseq = ckalloc( 2 * sizeof(sint *) );
           groupptr[i]->abstractseq[1] = ckalloc( (j+1) * sizeof (sint) );
           for(k=1;k<=j;k++) groupptr[i]->abstractseq[1][k] = k;
	   groupptr[i]->abstractseq[1][0] = i;
        }


	/* JP: lib generation */
        /* allocation for glib, a three dimensional array */    /************* step 9 ***************/
      if(ngroups>0) {
	glib = ckalloc(ngroups*sizeof(SN ***) );
	for(i=0;i<ngroups;i++) {
	  glib[i] = ckalloc(ngroups*sizeof(SN **));
	  for(j=0;j<ngroups;j++)
	  	glib[i][j] = ckalloc( (groupptr[i]->seqlength+1)*sizeof(SN *));
	}



	fprintf(stdout, "Start generating libray\n");
	lib_generation();  /************* step 10 ***************/
	/*printLib(0,1);
	printLib(1,0);
 	printLib(0,2);
	printLib(2,0);
	printLib(1,2);
	printLib(2,1); */ 


	if(debug>1)fprintf(stdout, "++++++++++++\n"); fflush(stdout);

	/* JP: lib extension */   /************* step 11 ***************/
	fprintf(stdout, "\n\nStart extending library\n\n");
	lib_extension();
      }

 	/* printMatrix(0,1); */
	/* exit(0); */

	/* JP: progressive alignment of the abstract sequences */  /************* step 12 ***************/
	fprintf(stdout, "Start progressive alignment of pre-aligned groups\n");
	for(set=1;set<=nsets;set++) {
	    if( (!grp_ancestor[set]->left->abstractseq)&&(!grp_ancestor[set]->right->abstractseq) ) {
		continue;
	    }
	    if((grp_ancestor[set]->left->abstractseq)&&(grp_ancestor[set]->right->abstractseq) ) {
		if(debug>1)fprintf(stdout, "SET: %d\n", set);
		prfalignabs(set); continue;
	    }
	    if((grp_ancestor[set]->left->abstractseq)&&(!grp_ancestor[set]->right->abstractseq) ) {
		fprintf(stdout,"Error: abstract sequence of the right child do not exist. set %d\n", set);
		exit(0);
	    }
	    if((!grp_ancestor[set]->left->abstractseq)&&(!grp_ancestor[set]->right->abstractseq) ) {
		fprintf(stdout, "Error: abstract sequence of the left child do not exist. set %d\n", set);
		exit(0);
	    }
	}

	/* change the seq_array */
      if(seqFormat!=CLUSTALIST) { 
	tmpseq_array = ckalloc( (nseqs+1) *sizeof(sint *));
	for(j=1;j<=nseqs;j++) {
	   tmpseq_array[j] = ckalloc((seqlen_array[j]+1)*sizeof(sint));
	   for(i=1;i<=seqlen_array[j];i++) {
		tmpseq_array[j][i] = seq_array[j][i];
	   }
	}
	for(j=1;j<=grp_ancestor[nsets]->abseqnum;j++) {
	   nd = groupptr[grp_ancestor[nsets]->abstractseq[j][0]];

	   for(i=1;i<=nd->seqnum;i++) {
		for(k=1;k<=nseqs;k++) {
		   if(!strcmp(nd->name[i], names[k]) ) break;
		}
		if(k>nseqs) {
		   fprintf(stdout, "Error: name does not match: %s\n", nd->name[i]);
		   exit(0);
		}
		seqlen_array[k] = grp_ancestor[nsets]->abseqlength;
		realloc_seq(k, seqlen_array[k]);
		for(l=1;l<=seqlen_array[k];l++) {
		   if(grp_ancestor[nsets]->abstractseq[j][l]==0) {
			seq_array[k][l] = gap_pos1;
			if(debug>1)fprintf(stdout, "-");
		   }
		   else {
			seq_array[k][l] = tmpseq_array[k][grp_ancestor[nsets]->abstractseq[j][l]];
			/*seq_array[k][l] = nd->seq[i][grp_ancestor[nsets]->abstractseq[j][l]];*/
			if(debug>1)fprintf(stdout, "%c", am[nd->seq[i][grp_ancestor[nsets]->abstractseq[j][l]]]);
		   }
		   /*if(seq_array[k][l]==0) {
			seq_array[k][l] = gap_pos1;
		   }
		   else {
			seq_array[k][l] = res_index(amino_acid_codes, am[seq_array[k][l]]);
		   }*/
		}
	    }
	}

	for(j=1;j<=nseqs;j++) ckfree(tmpseq_array[j]);
	ckfree(tmpseq_array);
      }
      /* JP: for_align_list */
      else {
	int Nd, tmpcount;
	int mark;
	tmpnames = ckalloc((filecount+1) * sizeof(char *) );
	for(j=1;j<=filecount;j++) tmpnames[j] = ckalloc((100+1) * sizeof(char));
	for(j=1;j<=filecount;j++) strcpy(tmpnames[j], names[j]);
	free_aln(filecount);
	alloc_aln(nseqs_all);

	//fprintf(stdout, "%d %d\n", nseqs_all, grp_ancestor[nsets]->abseqnum);

	for(j=1;j<=grp_ancestor[nsets]->abseqnum;j++) {
	   mark = 0; /* the index of the corresponding filecount */
	   tmpcount = 0; /* the starting index of the group */
           nd = groupptr[grp_ancestor[nsets]->abstractseq[j][0]];
	   /* locate the index of the subalignment */
	   for(i=1;i<=filecount;i++) {
		for(k=1;k<=nd->seqnum;k++) {
		     if(debug>11) fprintf(stdout, "%d %s %d %s\n", k, nd->name[k], i, tmpnames[i]);
		     if(strcmp(nd->name[k], tmpnames[i])==0) {mark = i; break;}
		}
		if(mark > 0) break;
	   }
	   if(debug>11) fprintf(stdout, "%d\n", mark);
	   for(i=1;i<=mark-1;i++) tmpcount += seqnumlist[i];

           for(i=1;i<=seqnumlist[mark];i++) {
		k = tmpcount + i;
		seqlen_array[k] = grp_ancestor[nsets]->abseqlength;
		alloc_seq(k, seqlen_array[k]);
		strcpy(names[k], names_all[k]);
		for(l=1;l<=seqlen_array[k];l++) {
                   if(grp_ancestor[nsets]->abstractseq[j][l]==0) {
                        seq_array[k][l] = gap_pos1;
                        if(debug>11)fprintf(stdout, "-");
                   }
                   else {
                        seq_array[k][l] = seq_array_all[k][grp_ancestor[nsets]->abstractseq[j][l]];
                        /*seq_array[k][l] = nd->seq[i][grp_ancestor[nsets]->abstractseq[j][l]];*/
                        if(debug>11)fprintf(stdout, "%c", am[nd->seq[i][grp_ancestor[nsets]->abstractseq[j][l]]]);
                   }
		}
		if(debug>11) fprintf(stdout, "\n");
	   }
        }
	for(i=1;i<=nseqs_all;i++) {
              if(strlen(names[i])>max_names) max_names=strlen(names[i]);
        }
     }

/* second pass - align remaining, more divergent sequences..... */

/* if not all sequences were aligned, for each unaligned sequence,
   find it's closest pair amongst the aligned sequences.  */

    group = (sint *)ckalloc( (nseqs+1) * sizeof (sint));
    tree_weight = (sint *) ckalloc( (nseqs) * sizeof(sint) );
    for (i=0;i<nseqs;i++)
   		tree_weight[i] = seq_weight[i];

/* if we haven't aligned any sequences, in the first pass - align the
two most closely related sequences now */
   if(ix==0)
     {
        max = -1;
	iseq = 0;
        for (i=1;i<=nseqs;i++)
          {
             for (j=i+1;j<=nseqs;j++)
               {
                  if (max < tmat[i][j])
		  {
                     max = tmat[i][j];
                     iseq = i;
                  }
              }
          }
        aligned[iseq]=1;
        if (output_order == INPUT)
          {
            ++ix;
            output_index[iseq] = iseq;
          }
         else
            output_index[++ix] = iseq;
     }

    while (ix < nseqs)
      {
             for (i=1;i<=nseqs;i++) {
                if (aligned[i] == 0)
                  {
                     maxid[i] = -1;
                     for (j=1;j<=nseqs;j++)
                        if ((maxid[i] < tmat[i][j]) && (aligned[j] != 0))
                            maxid[i] = tmat[i][j];
                  }
              }
/* find the most closely related sequence to those already aligned */

            max = -1;
	    iseq = 0;
            for (i=1;i<=nseqs;i++)
              {
                if ((aligned[i] == 0) && (maxid[i] > max))
                  {
                     max = maxid[i];
                     iseq = i;
                  }
              }


/* align this sequence to the existing alignment */
/* weight sequences with percent identity with profile*/
/* OR...., multiply sequence weights from tree by percent identity with new sequence */
   if(no_weights==FALSE) {
   for (j=0;j<nseqs;j++)
       if (aligned[j+1] != 0)
              seq_weight[j] = tree_weight[j] * tmat[j+1][iseq];
/*
  Normalise the weights, such that the sum of the weights = INT_SCALE_FACTOR
*/

         sum = 0;
         for (j=0;j<nseqs;j++)
           if (aligned[j+1] != 0)
            sum += seq_weight[j];
         if (sum == 0)
          {
           for (j=0;j<nseqs;j++)
                seq_weight[j] = 1;
                sum = j;
          }
         for (j=0;j<nseqs;j++)
           if (aligned[j+1] != 0)
             {
               seq_weight[j] = (seq_weight[j] * INT_SCALE_FACTOR) / sum;
               if (seq_weight[j] < 1) seq_weight[j] = 1;
             }
	}

         entries = 0;
         for (j=1;j<=nseqs;j++)
           if (aligned[j] != 0)
              {
                 group[j] = 1;
                 entries++;
              }
           else if (iseq==j)
              {
                 group[j] = 2;
                 entries++;
              }
         aligned[iseq] = 1;

         score = prfalign(group, aligned);
         info("Sequence:%d     Score:%d",(pint)iseq,(pint)score);
         if (output_order == INPUT)
          {
            ++ix;
            output_index[iseq] = iseq;
          }
         else
            output_index[++ix] = iseq;
      }

   group=ckfree((void *)group);
   aligned=ckfree((void *)aligned);
   maxid=ckfree((void *)maxid);
   tree_weight=ckfree((void *)tree_weight);

   /* aln_score(); */

/* make the rest (output stuff) into routine clustal_out in file amenu.c */

   return(nseqs);

}

sint seqalign(sint istart,char *phylip_name) /* sequence alignment to existing profile */
{
   static 	sint *aligned, *tree_weight;
   static 	sint *group;
   static 	sint ix;

   sint 	*maxid, max;
   sint 		i,j,status,iseq;
   sint 		sum,entries;
   lint		score = 0;


   info("Start making multiple alignment\n");

/* get the phylogenetic tree from *.ph */

   if (nseqs >= 2)
     {
       status = read_tree(phylip_name, (sint)0, nseqs);
       if (status == 0) return(0);
     }

/* calculate sequence weights according to branch lengths of the tree -
   weights in global variable seq_weight normalised to sum to 100 */

   calc_seq_weights((sint)0, nseqs, seq_weight);

   tree_weight = (sint *) ckalloc( (nseqs) * sizeof(sint) );
   for (i=0;i<nseqs;i++)
   		tree_weight[i] = seq_weight[i];

/* recalculate tmat matrix as percent similarity matrix */

   status = calc_similarities(nseqs);
   if (status == 0) return((sint)0);

/* for each sequence, find the most closely related sequence */

   maxid = (sint *)ckalloc( (nseqs+1) * sizeof (sint));
   for (i=1;i<=nseqs;i++)
     {
         maxid[i] = -1;
         for (j=1;j<=nseqs;j++)
           if (maxid[i] < tmat[i][j]) maxid[i] = tmat[i][j];
     }

/* clear the memory used for the phylogenetic tree */

        if (nseqs >= 2)
             clear_tree(NULL);

        aligned = (sint *)ckalloc( (nseqs+1) * sizeof (sint) );
        ix = 0;
        for (i=1;i<=istart+1;i++)
         {
           aligned[i] = 1;
           ++ix;
           output_index[i] = i;
         }
        for (i=istart+2;i<=nseqs;i++) aligned[i] = 0;

/* for each unaligned sequence, find it's closest pair amongst the
   aligned sequences.  */

    group = (sint *)ckalloc( (nseqs+1) * sizeof (sint));

    while (ix < nseqs)
      {
        if (ix > 0)
          {
             for (i=1;i<=nseqs;i++) {
                if (aligned[i] == 0)
                  {
                     maxid[i] = -1;
                     for (j=1;j<=nseqs;j++)
                        if ((maxid[i] < tmat[i][j]) && (aligned[j] != 0))
                            maxid[i] = tmat[i][j];
                  }
              }
          }

/* find the most closely related sequence to those already aligned */

         max = -1;
         for (i=1;i<=nseqs;i++)
           {
             if ((aligned[i] == 0) && (maxid[i] > max))
               {
                  max = maxid[i];
                  iseq = i;
               }
           }

/* align this sequence to the existing alignment */

         entries = 0;
         for (j=1;j<=nseqs;j++)
           if (aligned[j] != 0)
              {
                 group[j] = 1;
                 entries++;
              }
           else if (iseq==j)
              {
                 group[j] = 2;
                 entries++;
              }
         aligned[iseq] = 1;


/* EITHER....., set sequence weights equal to percent identity with new sequence */
/*
           for (j=0;j<nseqs;j++)
              seq_weight[j] = tmat[j+1][iseq];
*/
/* OR...., multiply sequence weights from tree by percent identity with new sequence */
           for (j=0;j<nseqs;j++)
              seq_weight[j] = tree_weight[j] * tmat[j+1][iseq];
if (debug>1)
  for (j=0;j<nseqs;j++) if (group[j+1] == 1)fprintf (stdout,"sequence %d: %d\n", j+1,tree_weight[j]);
/*
  Normalise the weights, such that the sum of the weights = INT_SCALE_FACTOR
*/

         sum = 0;
         for (j=0;j<nseqs;j++)
           if (group[j+1] == 1) sum += seq_weight[j];
         if (sum == 0)
          {
           for (j=0;j<nseqs;j++)
                seq_weight[j] = 1;
                sum = j;
          }
         for (j=0;j<nseqs;j++)
             {
               seq_weight[j] = (seq_weight[j] * INT_SCALE_FACTOR) / sum;
               if (seq_weight[j] < 1) seq_weight[j] = 1;
             }

if (debug > 1) {
  fprintf(stdout,"new weights\n");
  for (j=0;j<nseqs;j++) if (group[j+1] == 1)fprintf( stdout,"sequence %d: %d\n", j+1,seq_weight[j]);
}

         score = prfalign(group, aligned);
         info("Sequence:%d     Score:%d",(pint)iseq,(pint)score);
         if (output_order == INPUT)
          {
            ++ix;
            output_index[iseq] = iseq;
          }
         else
            output_index[++ix] = iseq;
      }

   group=ckfree((void *)group);
   aligned=ckfree((void *)aligned);
   maxid=ckfree((void *)maxid);

   /* aln_score(); */

/* make the rest (output stuff) into routine clustal_out in file amenu.c */

   return(nseqs);

}


sint palign1(void)  /* a profile alignment */
{
   sint 		i,j,temp;
   sint 		entries;
   sint 		*aligned, *group;
   float        dscore;
   lint			score;

   info("Start of Initial Alignment\n");

/* calculate sequence weights according to branch lengths of the tree -
   weights in global variable seq_weight normalised to sum to INT_SCALE_FACTOR */

   temp = INT_SCALE_FACTOR/nseqs;
   for (i=0;i<nseqs;i++) seq_weight[i] = temp;

   distance_tree = FALSE;

/* do the initial alignment.........  */

   group = (sint *)ckalloc( (nseqs+1) * sizeof (sint));

   for(i=1; i<=profile1_nseqs; ++i)
         group[i] = 1;
   for(i=profile1_nseqs+1; i<=nseqs; ++i)
         group[i] = 2;
   entries = nseqs;

   aligned = (sint *)ckalloc( (nseqs+1) * sizeof (sint) );
   for (i=1;i<=nseqs;i++) aligned[i] = 1;

   score = prfalign(group, aligned);
   info("Sequences:%d      Score:%d",(pint)entries,(pint)score);
   group=ckfree((void *)group);
   aligned=ckfree((void *)aligned);

   for (i=1;i<=nseqs;i++) {
     for (j=i+1;j<=nseqs;j++) {
       dscore = countid(i,j);
       tmat[i][j] = ((double)100.0 - (double)dscore)/(double)100.0;
       tmat[j][i] = tmat[i][j];
     }
   }

   return(nseqs);
}

float countid(sint s1, sint s2)
{
   char c1,c2;
   sint i;
   sint count,total;
   float score;

   count = total = 0;
   for (i=1;i<=seqlen_array[s1] && i<=seqlen_array[s2];i++) {
     c1 = seq_array[s1][i];
     c2 = seq_array[s2][i];
     if ((c1>=0) && (c1<max_aa)) {
       total++;
       if (c1 == c2) count++;
     }

   }

   if(total==0) score=0;
   else
   score = 100.0 * (float)count / (float)total;
   return(score);

}

sint palign2(char *p1_tree_name,char *p2_tree_name)  /* a profile alignment */
{
   sint 	i,j,sum,entries,status;
   lint 		score;
   sint 	*aligned, *group;
   sint		*maxid,*p1_weight,*p2_weight;
   sint dscore;

   info("Start making multiple alignment\n");

/* get the phylogenetic trees from *.ph */

   if (profile1_nseqs >= 2)
     {
        status = read_tree(p1_tree_name, (sint)0, profile1_nseqs);
        if (status == 0) return(0);
     }

/* calculate sequence weights according to branch lengths of the tree -
   weights in global variable seq_weight normalised to sum to 100 */

   p1_weight = (sint *) ckalloc( (profile1_nseqs) * sizeof(sint) );

   calc_seq_weights((sint)0, profile1_nseqs, p1_weight);

/* clear the memory for the phylogenetic tree */

   if (profile1_nseqs >= 2)
        clear_tree(NULL);

   if (nseqs-profile1_nseqs >= 2)
     {
        status = read_tree(p2_tree_name, profile1_nseqs, nseqs);
        if (status == 0) return(0);
     }

   p2_weight = (sint *) ckalloc( (nseqs) * sizeof(sint) );

   calc_seq_weights(profile1_nseqs,nseqs, p2_weight);


/* clear the memory for the phylogenetic tree */

   if (nseqs-profile1_nseqs >= 2)
        clear_tree(NULL);

/* convert tmat distances to similarities */

   for (i=1;i<nseqs;i++)
        for (j=i+1;j<=nseqs;j++) {
            tmat[i][j]=100.0-tmat[i][j]*100.0;
            tmat[j][i]=tmat[i][j];
        }


/* weight sequences with max percent identity with other profile*/

   maxid = (sint *)ckalloc( (nseqs+1) * sizeof (sint));
   for (i=0;i<profile1_nseqs;i++) {
         maxid[i] = 0;
         for (j=profile1_nseqs+1;j<=nseqs;j++)
                      if(maxid[i]<tmat[i+1][j]) maxid[i] = tmat[i+1][j];
         seq_weight[i] = maxid[i]*p1_weight[i];
   }

   for (i=profile1_nseqs;i<nseqs;i++) {
         maxid[i] = -1;
         for (j=1;j<=profile1_nseqs;j++)
                      if(maxid[i]<tmat[i+1][j]) maxid[i] = tmat[i+1][j];
         seq_weight[i] = maxid[i]*p2_weight[i];
   }
/*
  Normalise the weights, such that the sum of the weights = INT_SCALE_FACTOR
*/

         sum = 0;
         for (j=0;j<nseqs;j++)
            sum += seq_weight[j];
         if (sum == 0)
          {
           for (j=0;j<nseqs;j++)
                seq_weight[j] = 1;
                sum = j;
          }
         for (j=0;j<nseqs;j++)
             {
               seq_weight[j] = (seq_weight[j] * INT_SCALE_FACTOR) / sum;
               if (seq_weight[j] < 1) seq_weight[j] = 1;
             }
if (debug > 1) {
  fprintf(stdout,"new weights\n");
  for (j=0;j<nseqs;j++) fprintf( stdout,"sequence %d: %d\n", j+1,seq_weight[j]);
}


/* do the alignment.........  */

   /* JP: disable info */
   /*info("Aligning...");*/

   group = (sint *)ckalloc( (nseqs+1) * sizeof (sint));

   for(i=1; i<=profile1_nseqs; ++i)
         group[i] = 1;
   for(i=profile1_nseqs+1; i<=nseqs; ++i)
         group[i] = 2;
   entries = nseqs;

   aligned = (sint *)ckalloc( (nseqs+1) * sizeof (sint) );
   for (i=1;i<=nseqs;i++) aligned[i] = 1;

   score = prfalign(group, aligned);
   info("Sequences:%d      Score:%d",(pint)entries,(pint)score);
   group=ckfree((void *)group);
   p1_weight=ckfree((void *)p1_weight);
   p2_weight=ckfree((void *)p2_weight);
   aligned=ckfree((void *)aligned);
   maxid=ckfree((void *)maxid);

/* DES   output_index = (int *)ckalloc( (nseqs+1) * sizeof (int)); */
   for (i=1;i<=nseqs;i++) output_index[i] = i;

   return(nseqs);
}

