#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "pcma.h"
/*#include "new.h"*/
#define ENDALN 127

#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))

/*
 *   Prototypes
 */
static lint 	pdiff1(sint A,sint B,sint M,sint N,sint go1, sint go2);
static lint 	absprfscore(sint set, sint n, sint m);
static sint 	open_penalty1(sint i, sint j);
static void 	padd(sint k);
static void 	pdel(sint k);
static void 	palign(void);
static void 	ptracepath(sint *alen);
static void 	add_ggaps(void);
static char *     add_ggaps_mask(char *mask, int len, char *path1, char *path2);

/*
 *   Global variables
 */
extern double 	**tmat;
extern float 	gap_open, gap_extend;
extern float    transition_weight;
extern sint 	gap_pos1, gap_pos2;
extern sint 	max_aa;
extern sint 	nseqs;
extern sint 	*seqlen_array;
extern sint 	*seq_weight;
extern sint    	debug;
extern Boolean 	neg_matrix;
extern sint 	mat_avscore;
extern short  	blosum30mt[], blosum40mt[], blosum45mt[];
extern short  	blosum62mt2[], blosum80mt[];
extern short  	pam20mt[], pam60mt[];
extern short  	pam120mt[], pam160mt[], pam350mt[];
extern short  	gon40mt[], gon80mt[];
extern short    gon120mt[], gon160mt[], gon250mt[], gon350mt[];
extern short  	clustalvdnamt[],swgapdnamt[];
extern short  	idmat[];
extern short    usermat[];
extern short    userdnamat[];
extern Boolean user_series;
extern UserMatSeries matseries;

extern short	def_dna_xref[],def_aa_xref[],dna_xref[],aa_xref[];
extern sint		max_aln_length;
extern Boolean	distance_tree;
extern Boolean	dnaflag;
extern char 	mtrxname[];
extern char 	dnamtrxname[];
extern char 	**seq_array;
extern char 	*amino_acid_codes;
extern char     *gap_penalty_mask1,*gap_penalty_mask2;
extern char     *sec_struct_mask1,*sec_struct_mask2;
extern sint     struct_penalties1, struct_penalties2;
extern Boolean  use_ss1, use_ss2;
extern Boolean endgappenalties;

/* JP */
extern sint        ngroups;
extern streeptr *groupptr;
extern void assign_node(streeptr p, sint *aligned);
extern streeptr *grp_ancestor;
extern sint ngroups;
extern sint ave_grp_id;
extern SN **** glib;
extern sint **sets;
extern sint cosmetic_penalty;
static sint setnum;
static sint go;
static sint ge;



static sint 	print_ptr,last_print;
static sint 		*displ;

static char   	**alignment;
static sint    	*aln_len;
static sint    *aln_weight;
static char   	*aln_path1, *aln_path2;
static sint    	alignment_len;
static sint    	**profile1, **profile2;
static lint 	    *HH, *DD, *RR, *SS;
static lint 	    *gS;
static sint    matrix[NUMRES][NUMRES];
static sint    nseqs1, nseqs2;
static sint    	prf_length1, prf_length2;
static sint    *gaps;
static sint    gapcoef1,gapcoef2;
static sint    lencoef1,lencoef2;
static Boolean switch_profiles;

lint prfalignabs(sint set)
{

  static Boolean found;
  static Boolean negative;
  static Boolean error_given=FALSE;
  static sint    i, j, count = 0;
  static sint  NumSeq;
  static sint    len, len1, len2, is, minlen;
  static sint   se1, se2, sb1, sb2;
  static sint  maxres;
  static sint int_scale;
  static short  *matptr;
  static short	*mat_xref;
  static char   c;
  static lint    score;
  static float  scale;
  static double logmin,logdiff;
  static double pcid;

  setnum = set;
  go = cosmetic_penalty; ge = 0;

  nseqs1 = grp_ancestor[set]->left->abseqnum;
  nseqs2 = grp_ancestor[set]->right->abseqnum; 
  grp_ancestor[set]->abseqnum = nseqs1 + nseqs2;
  grp_ancestor[set]->abstractseq = ckalloc( (nseqs1 + nseqs2 + 2) * sizeof(int *));
  /* fprintf(stdout, "nseqs1: %d nseqs2: %d abs: %d\n", nseqs1, nseqs2, grp_ancestor[set]->abseqnum); */

/*
   align the profiles
*/
/* use Myers and Miller to align two sequences */

  last_print = 0;
  print_ptr = 1;

  sb1 = sb2 = 0;
  se1 = grp_ancestor[set]->left->abseqlength;
  se2 = grp_ancestor[set]->right->abseqlength;

  if(debug > 1)fprintf(stdout, "se1: %d; se2: %d\n", se1, se2);

  max_aln_length = se1 + se2 + 2;

  aln_path1 = (char *) ckalloc( (max_aln_length+1) * sizeof(char) );
  aln_path2 = (char *) ckalloc( (max_aln_length+1) * sizeof(char) );


  HH = (lint *) ckalloc( (max_aln_length+1) * sizeof (lint) );
  DD = (lint *) ckalloc( (max_aln_length+1) * sizeof (lint) );
  RR = (lint *) ckalloc( (max_aln_length+1) * sizeof (lint) );
  SS = (lint *) ckalloc( (max_aln_length+1) * sizeof (lint) );
  gS = (lint *) ckalloc( (max_aln_length+1) * sizeof (lint) );
  displ = (sint *) ckalloc( (max_aln_length+1) * sizeof (sint) );

if(debug > 1) {fprintf(stdout, "gap penalties: %d %d %d \n", go, ge, cosmetic_penalty);

  fprintf(stdout, "SET: set\n"); 
  if(set==1111) {
     for(i=1;i<=100;i++) {
	for(j=1;j<=100;j++) {
	   if(absprfscore(set, i,j)==0) fprintf(stdout, "-");
	   else 
	   fprintf(stdout, "%d", absprfscore(set, i,j)) ;
	}
	fprintf(stdout, "\n");
     }
  }

}

  score = pdiff1(sb1, sb2, se1-sb1, se2-sb2, cosmetic_penalty, cosmetic_penalty);
  if(debug>1) fprintf(stdout, "%d\n", score);


  HH=ckfree((void *)HH);
  DD=ckfree((void *)DD);
  RR=ckfree((void *)RR);
  SS=ckfree((void *)SS);
  gS=ckfree((void *)gS);
 
  // NEW: test the displ
  if(displ[1]==0) {
	displ[1] = displ[2];
	displ[2] = 0;
  }

  ptracepath( &alignment_len);
  /*fprintf(stdout, "print_ptr: %d\n", print_ptr);*/
  /*for(i=0;i<=print_ptr;i++) {
	fprintf(stdout, "%d\t%d\n", i, displ[i]);
  }*/
  
  displ=ckfree((void *)displ);

 if(debug>1)
  for(i=0;i<alignment_len;i++) {
     fprintf(stdout, "path %d: %d %d\n", i, aln_path1[i], aln_path2[i]);
  }

  add_ggaps();

  grp_ancestor[set]->abseqlength = alignment_len;
	
  /*fprintf(stdout, "\n");*/

  /*fprintf(stdout, "%d %d\n", grp_ancestor[set]->left->abstractseq[1][0],
	grp_ancestor[set]->right->abstractseq[1][0]);
  fprintf(stdout, "%d %d\n", grp_ancestor[set]->abstractseq[1][0],
        grp_ancestor[set]->abstractseq[2][0]);
  */

  if(debug>1)printAbstract(set);
  fflush(stdout);

  fflush(stdout);
  aln_path1=ckfree((void *)aln_path1);
  aln_path2=ckfree((void *)aln_path2);  
  return 0; 

  /*exit(0);*/

}

static void add_ggaps(void)
{
   sint j;
   sint i,ix;
   sint len;
   char *ta;

   /*fprintf(stdout, "%d %d\n", setnum, grp_ancestor[setnum]->abseqnum);*/
   if(debug>1)fprintf(stdout, "#########alignment length: %d\n", alignment_len);
   /*grp_ancestor[setnum]->abstractseq = ckalloc( (nseqs1 + nseqs2 + 2) * sizeof(int *));*/
   for(i=1;i<=grp_ancestor[setnum]->abseqnum;i++) {
     grp_ancestor[setnum]->abstractseq[i] = ckalloc((alignment_len+3)*sizeof(sint));
   }

   for(j=1;j<=nseqs1;j++) {
     /*fprintf(stdout, "%d ", grp_ancestor[setnum]->left->abstractseq[j][0]);*/
     grp_ancestor[setnum]->abstractseq[j][0] = grp_ancestor[setnum]->left->abstractseq[j][0];
     ix = 1;
     /*for(i=0;i<alignment_len;i++) {
        fprintf(stdout, "i: %d ; %d %d \n", i, aln_path1[i], aln_path2[i]); fflush(stdout);
     }*/
     for(i=0;i<alignment_len;i++) {
   	/*fprintf(stdout, "i: %d ; %d \n", i, aln_path1[i]); fflush(stdout);*/
	if(aln_path1[i]==2) {
		/*fprintf(stdout, "ix: %d;  %d\n", ix, grp_ancestor[setnum]->left->abstractseq[j][ix]);*/
		grp_ancestor[setnum]->abstractseq[j][i+1]=grp_ancestor[setnum]->left->abstractseq[j][ix];
		ix++;
	}
	else if(aln_path1[i]==1) {
	    grp_ancestor[setnum]->abstractseq[j][i+1]==0;
	}
	else {
	    fprintf(stderr, "Error in aln_path: %d\n", aln_path1[i]); exit(0);
	}
     }
   }

     for(j=nseqs1+1;j<=nseqs1+nseqs2;j++) {
	grp_ancestor[setnum]->abstractseq[j][0]=grp_ancestor[setnum]->right->abstractseq[j-nseqs1][0];
	ix = 1;
	for(i=0;i<alignment_len;i++) {
	   if(aln_path2[i]==2) {
		grp_ancestor[setnum]->abstractseq[j][i+1]=grp_ancestor[setnum]->right->abstractseq[j-nseqs1][ix];
		ix++;
	   }
	   else if(aln_path2[i]==1) {
		grp_ancestor[setnum]->abstractseq[j][i+1]==0;
	   }
	   else {
		fprintf(stderr, "Error in aln path: %d\n", aln_path2[i]); exit(0);
	   }
	}
    }

    for(i=1;i<=nseqs1;i++) 
	ckfree(grp_ancestor[setnum]->left->abstractseq[i]);
    for(j=1;j<=nseqs2;j++)
	ckfree(grp_ancestor[setnum]->right->abstractseq[j]);
    ckfree(grp_ancestor[setnum]->left->abstractseq);
    ckfree(grp_ancestor[setnum]->right->abstractseq);
}                  

static char * add_ggaps_mask(char *mask, int len, char *path1, char *path2)
{
   int i,ix;
   char *ta;

   ta = (char *) ckalloc( (len+1) * sizeof (char) );

       ix = 0;
       if (switch_profiles == FALSE)
        {     
         for (i=0;i<len;i++)
           {
             if (path1[i] == 2)
              {
                ta[i] = mask[ix];
                ix++;
              }
             else if (path1[i] == 1)
                ta[i] = gap_pos1;
           }
        }
       else
        {
         for (i=0;i<len;i++)
          {
            if (path2[i] == 2)
             {
               ta[i] = mask[ix];
               ix++;
             }
            else if (path2[i] == 1)
             ta[i] = gap_pos1;
           }
         }
       mask = (char *)realloc(mask,(len+2) * sizeof (char));
       for (i=0;i<len;i++)
         mask[i] = ta[i];
       mask[i] ='\0';
       
   ta=ckfree((void *)ta);

   return(mask);
}

/*static lint absprfscore(sint set, sint n, sint m)
{
   sint    ix;
   lint  score;
   sint    i,j,k;
   sint gid1, gid2;
   SN *nd;

   score = 0;
   for(i=1;i<=nseqs1;i++) {
     if(grp_ancestor[set]->left->abstractseq[i][n]==0) continue;
     gid1 = grp_ancestor[set]->left->abstractseq[i][0];
     for(j=1;j<=nseqs2;j++) {
	if(grp_ancestor[set]->right->abstractseq[j][m]==0) continue;
	gid2 = grp_ancestor[set]->right->abstractseq[j][0];
	nd = glib[gid1][gid2][grp_ancestor[set]->left->abstractseq[i][n]];
	while(nd!=NULL) {
	   if(nd->ind == grp_ancestor[set]->right->abstractseq[j][m]) {
		score += nd->sae;
		break;
	   }
	   else nd = nd->next;
	}
     }
   }
	
   return(score);
}
*/


static lint absprfscore(sint set, sint n, sint m)
{
   sint    ix;
   lint  score;
   sint    i,j,k;
   sint gid1, gid2;
   SN *nd;

   score = 0;
   for(i=1;i<=nseqs1;i++) {
     if(grp_ancestor[set]->left->abstractseq[i][n]==0) continue;
     gid1 = grp_ancestor[set]->left->abstractseq[i][0];
     for(j=1;j<=nseqs2;j++) {
        if(grp_ancestor[set]->right->abstractseq[j][m]==0) continue;
        gid2 = grp_ancestor[set]->right->abstractseq[j][0];
	if(gid1<gid2) {
           nd = glib[gid1][gid2][grp_ancestor[set]->left->abstractseq[i][n]];
           while(nd!=NULL) {
              if(nd->ind == grp_ancestor[set]->right->abstractseq[j][m]) {
                score += nd->sae;
                break;
              }
              else nd = nd->next;
           }
	}

	else {
	    nd = glib[gid2][gid1][grp_ancestor[set]->right->abstractseq[j][m]];
	    while(nd!=NULL) {
		if(nd->ind == grp_ancestor[set]->left->abstractseq[i][n]) {
		   score += nd->sae;
		   break;
		}
		else nd = nd->next;
	    }
	}
     }
   }

   return(score);
}


static void ptracepath(sint *alen)
{
    sint i,j,k,pos,to_do;

    pos = 0;

    to_do=print_ptr-1;

    for(i=1;i<=to_do;++i) {
if (debug>1) fprintf(stdout,"%d ",(pint)displ[i]);
            if(displ[i]==0) {
                    aln_path1[pos]=2;
                    aln_path2[pos]=2;
                    ++pos;
            }
            else {
                    if((k=displ[i])>0) {
                            for(j=0;j<=k-1;++j) {
                                    aln_path2[pos+j]=2;
                                    aln_path1[pos+j]=1;
                            }
                            pos += k;
                    }
                    else {
                            k = (displ[i]<0) ? displ[i] * -1 : displ[i];
                            for(j=0;j<=k-1;++j) {
                                    aln_path1[pos+j]=2;
                                    aln_path2[pos+j]=1;
                            }
                            pos += k;
                    }
            }
    }
if (debug>1) fprintf(stdout,"\n");

   (*alen) = pos;

}

static void pdel(sint k)
{
        if(last_print<0)
                last_print = displ[print_ptr-1] -= k;
        else
                last_print = displ[print_ptr++] = -(k);
}

static void padd(sint k)
{

        if(last_print<0) {
                displ[print_ptr-1] = k;
                displ[print_ptr++] = last_print;
        }
        else
                last_print = displ[print_ptr++] = k;
}

static void palign(void)
{
        displ[print_ptr++] = last_print = 0;
}


static lint pdiff1(sint A,sint B,sint M,sint N,sint go1, sint go2)
{
        sint midi,midj,type;
        lint midh;

        static lint t, tl, g, h;

{		static sint i,j;
        static lint hh, f, e, s;

/* Boundary cases: M <= 1 or N == 0 */
if (debug>2) fprintf(stdout,"A %d B %d M %d N %d midi %d go1 %d go2 %d\n",
(pint)A,(pint)B,(pint)M,(pint)N,(pint)M/2,(pint)go1,(pint)go2);

/* if sequence B is empty....                                            */

        if(N<=0)  {

/* if sequence A is not empty....                                        */

                if(M>0) {

/* delete residues A[1] to A[M]                                          */

                        pdel(M);
                }
		return (0);
                /*return(-gap_penalty1(A,B,M)); */
        }

/* if sequence A is empty....                                            */

        if(M<=1) {
                if(M<=0) {

/* insert residues B[1] to B[N]                                          */

                        padd(N);
			return( 0 );
                        /* return(-gap_penalty2(A,B,N)); */
                }

/* if sequence A has just one residue....                                */

                if (go1 == 0)
                	/*midh =  -gap_penalty1(A+1,B+1,N); */
			midh = -(ge  + go + N*ge);
                else
			midh = -(go2 + ge + go + N*ge);
                	/*midh =  -gap_penalty2(A+1,B,1)-gap_penalty1(A+1,B+1,N); */

                midj = 0;
                for(j=1;j<=N;j++) {
                        /*hh = -gap_penalty1(A,B+1,j-1) + prfscore(A+1,B+j) -gap_penalty1(A+1,B+j+1,N-j); */
			if(j>1) hh = -(go+(j-1)*ge) + absprfscore(setnum, A+1, B+j);
			else hh = absprfscore(setnum, A+1, B+j);
			if(j<N) hh += -(go+(N-j)*ge);
			/* hh = -(go+(j-1)*ge) + absprfscore(setnum, A+1, B+j) -(go+(N-j)*ge); */
                        if(hh>midh) {
                                midh = hh;
                                midj = j;
                        }
                }

                if(midj==0) {
                        padd(N);
                        pdel(1);
                }
                else {
                        if(midj>1) padd(midj-1);
                        palign();
                        if(midj<N) padd(N-midj);
                }
                return midh;
        }


/* Divide sequence A in half: midi */

        midi = M / 2;  

/* In a forward phase, calculate all HH[j] and HH[j] */

        HH[0] = 0.0;
	t = -go; tl = -ge;
        for(j=1;j<=N;j++) {
		HH[j] = t = t-ge;
		DD[j] = t-go;
        }

	t = -go1;
        for(i=1;i<=midi;i++) {
                s = HH[0];
		HH[0] = hh = t = t-ge;
		f = t-go;

                for(j=1;j<=N;j++) {

			f = f - ge;
			if( (hh - go -ge) > f ) f = hh -go - ge;
			DD[j] = DD[j] - ge;
			if(DD[j] < HH[j] - go -ge) DD[j] = HH[j] -go - ge;
			hh = s + absprfscore(setnum, A+i, B+j);
			if(f>hh) hh = f;
			if(DD[j]>hh) hh = DD[j];

			s = HH[j];
			HH[j] = hh;


                }
        }

        DD[0]=HH[0];

/* In a reverse phase, calculate all RR[j] and SS[j] */

        RR[N]=0.0;
	t = -go;
        for(j=N-1;j>=0;j--) {
		RR[j] = t = t -ge;
		SS[j] = t -go;
        }

	t = -go2;
        for(i=M-1;i>=midi;i--) {
                s = RR[N];
		RR[N] = hh = t = t - ge;
		f = t - go;

                for(j=N-1;j>=0;j--) {

			f = f - ge;
			if(hh-go-ge > f) f = hh -go -ge;
			SS[j] = SS[j] - ge;
			if(RR[j] - go -ge > SS[j] ) SS[j] = RR[j] - go -ge;

			hh = s + absprfscore(setnum, A+i+1, B+j+1);
			if(f>hh) hh = f;
			if(SS[j] > hh) hh = SS[j];

			s = RR[j];
			RR[j] = hh;

                }
        }
        SS[N]=RR[N];

/* find midj, such that HH[j]+RR[j] or DD[j]+SS[j]+gap is the maximum */

        midh=HH[0]+RR[0];
        midj=0;
        type=1;
        for(j=0;j<=N;j++) {
                hh = HH[j] + RR[j];
                if(hh>=midh)
                        if(hh>midh || (HH[j]!=DD[j] && RR[j]==SS[j])) {
                                midh=hh;
                                midj=j;
                        }
        }

        for(j=N;j>=0;j--) {
                hh = DD[j] + SS[j] - go;
                if(hh>midh) {
                        midh=hh;
                        midj=j;
                        type=2;
                }
        }
}

/* Conquer recursively around midpoint                                   */


	/*fprintf(stdout, "%d %d %d %d %d %d %d \n", A, midi, M, B, midj, N, type);*/


        if(type==1) {             /* Type 1 gaps  */
if (debug>2) fprintf(stdout,"Type 1,1: midj %d\n",(pint)midj);
                pdiff1(A,B,midi,midj,go1,go);
if (debug>2) fprintf(stdout,"Type 1,2: midj %d\n",(pint)midj);
                pdiff1(A+midi,B+midj,M-midi,N-midj,go,go2);
        }
        else {
if (debug>2) fprintf(stdout,"Type 2,1: midj %d\n",(pint)midj);
                pdiff1(A,B,midi-1,midj,go1, 0);
                pdel(2);
if (debug>2) fprintf(stdout,"Type 2,2: midj %d\n",(pint)midj);
                pdiff1(A+midi+1,B+midj,M-midi-1,N-midj,0,go2);
        }

        return midh;       /* Return the score of the best alignment */
}


