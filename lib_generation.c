#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "pcma.h"
/*#include "new.h"*/

extern SN ****glib;
extern sint     *seqlen_array;
extern char    **seq_array;
extern sint        ngroups;
extern streeptr *groupptr;
extern streeptr *grp_ancestor;
extern sint KK;
extern sint debug;
extern sint nseqs;
extern char **names;

extern void generatematrix(int **align1, int alnlength1, int nali1, int indi);


static double **scoremt;
static int **iscoremt;

extern char *am;
long int totalSN = 0;

static sint av = 0;
static SN *snarray;

double **sumefflet, ***neffAa, ***pscnt;

void lib_generation()
{
	sint i,j,k,l;

	sint ni;

 	sint *grp;

 	sint local = 1;
        sint global = 1;
	 
 	sint prc=0, prc1=0;

	sumefflet = ckalloc ( (ngroups+1) * sizeof(double *) );
	for(i=0;i<ngroups;i++) 
	    sumefflet[i] = ckalloc ( (groupptr[i]->seqlength+1) * sizeof(double) );
	neffAa = ckalloc ( (ngroups+1) * sizeof(double **) );
	for(i=0;i<ngroups;i++) {
	    neffAa[i] = ckalloc ( (groupptr[i]->seqlength+1) * sizeof(double *) );
	    for(j=1;j<=groupptr[i]->seqlength;j++) {
		neffAa[i][j] = ckalloc ( 21 * sizeof(double) );
	    }
	}
	
	pscnt = ckalloc ( (ngroups+1) * sizeof(double **) );
	for(i=0;i<ngroups;i++) {
            pscnt[i] = ckalloc ( (groupptr[i]->seqlength+1) * sizeof(double *) );
            for(j=1;j<=groupptr[i]->seqlength;j++) {
                pscnt[i][j] = ckalloc ( 21 * sizeof(double) );
            }
        }

	//fprintf(stdout, "==\n"); fflush(stdout);

	for(i=0;i<ngroups;i++) {
	     generatematrix(groupptr[i]->seq, groupptr[i]->seqlength, groupptr[i]->seqnum, i);
	     prc1++;
	     if(prc1%LINELENGTH==0) { fprintf(stdout, "\n"); } // start a new line in the output
	
	     if(debug>1) {
		fprintf(stdout, "******* %d ********\n", i);
		for(j=1; j<=groupptr[i]->seqlength; j++) {
		   for(k=1;k<=20;k++) {
			fprintf(stdout, "%8.5f", neffAa[i][j][k]);	
		   }
		   fprintf(stdout, "\n");
		   for(k=1;k<=20;k++) {
			 fprintf(stdout, "%8.5f", pscnt[i][j][k]);  
		   }
		   fprintf(stdout, "\n");
		}
		   fprintf(stdout, "\n");
	     }
	}
	fprintf(stdout, "\n");


	//fprintf(stdout, "==\n"); fflush(stdout);

	scoremt = NULL;
	iscoremt = NULL;

	/* allocate for the libs [0..ngroups-1][0..ngroups-1] */
	for(i=0;i<ngroups;i++) {
	  for(j=0;j<ngroups;j++) {
	    /*glib[i][j] = ckalloc(groupptr[i]->seqlength*sizeof(SN *) );*/
	    for(k=1;k<=groupptr[i]->seqlength;k++) 
		glib[i][j][k]=NULL;
	  }
	}

	for(i=0;i<ngroups;i++) {
          for(j=i+1;j<ngroups;j++) {


	    if(debug>1)fprintf(stdout, "Preparing lib for group i: %d j: %d\n", i, j);
            if(debug>1)fprintf(stdout, "seqnum: %d %d seqlength: %d %d\n", groupptr[i]->seqnum, groupptr[j]->seqnum, groupptr[i]->seqlength, groupptr[j]->seqlength);


	    /* allocate memory for the scoring matrix between two pre-aligned groups */
            scoremt = ckalloc( (groupptr[i]->seqlength+1)*sizeof(double *) );
	    for(k=1;k<=groupptr[i]->seqlength;k++)
		scoremt[k] = ckalloc( (groupptr[j]->seqlength+1)*sizeof(double));
	    iscoremt = ckalloc( (groupptr[i]->seqlength+1)*sizeof(int *) );
	    for(k=1;k<=groupptr[i]->seqlength;k++)
		iscoremt[k] = ckalloc( (groupptr[j]->seqlength+1)*sizeof(int));


	    /* profile-profile alignment to obtain the scoring matrix */
	    prfprfmatrix(i, j, groupptr[i]->seqlength, groupptr[j]->seqlength, groupptr[i]->seqnum, groupptr[j]->seqnum, scoremt); 

	    if(debug>1)fprintf(stdout, "++++++++++\n");

	    for(k=1;k<=groupptr[i]->seqlength;k++) {
	      for(l=1;l<=groupptr[j]->seqlength;l++) {
		iscoremt[k][l] = (int) scoremt[k][l]; 
	      }
	    }
		
	    if(debug>4) {
	      for(k=1;k<=groupptr[i]->seqlength;k++){
		fprintf(stdout, "i%d ", k);
		for(l=1;l<=groupptr[j]->seqlength;l++) {
		   fprintf(stdout, "%d ", iscoremt[k][l]);
		}
		fprintf(stdout, "\n");
	      }
	    }

	  if(local==1) {
	    /* lib generation */
	    fflush(stdout);
	    SIM(groupptr[i]->seqlength, groupptr[j]->seqlength, KK,iscoremt, 320,32, 2, i,j);

	    /* SIM(groupptr[j]->seqlength, groupptr[i]->seqlength, KK,iscoremt, 320 ,32, 2, j,i); */
	    /*SIM(groupptr[i]->seqlength, groupptr[j]->seqlength, KK,iscoremt, 320,128, 2, i,j); */
	  }


       if(global==1) {

      	  /* lib generation from global alignment */
	  grp = ckalloc( (nseqs+1) * sizeof(sint) );
    	  for(k=1;k<=nseqs;k++) grp[k] = 0;
	    for(k=1;k<=nseqs;k++) grp[k] = 0;

	    for(ni=1;ni<=groupptr[i]->seqnum;ni++) {
		for(k=1;k<=nseqs;k++) {
		   if(!strcmp(names[k], groupptr[i]->name[ni]) ) {
			grp[k] = 1;
			break;
		   }
		}
	    }
	    for(ni=1;ni<=groupptr[j]->seqnum;ni++) {
		for(k=1;k<=nseqs;k++) {
		   if(!strcmp(names[k], groupptr[j]->name[ni]) ) {
			grp[k] = 2;
			break;
		   }
		}
	    }

	    if(debug>1)fprintf(stdout, "i %d j %d ============\n",i,j); fflush(stdout);
	    prfalign1(grp, i, j);
	    /* prfalign2(grp, i, j, 352, 32, iscoremt); */
	   ckfree((void *)grp);
    	} 

	for(k=1;k<=groupptr[i]->seqlength;k++){
		ckfree((void *)scoremt[k]);
		ckfree((void *)iscoremt[k]);
	}
	ckfree((void *)scoremt);
	ckfree((void *)iscoremt);

	prc++;
	if((prc%LINELENGTH)==0) fprintf(stdout, "\n");
     }
  }

}

/* SN * SNavail(){
	SN *cur = ckalloc(sizeof(SN));
	cur->sbe = 0;
	cur->sae = 0;
	cur->ind = 0;
	cur->next = NULL;
	totalSN++;
	if(totalSN%100000==0) {
	  if(debug>1) fprintf(stdout, "memory for SN: %d * %d\n", totalSN, sizeof(SN) );
	}
	return cur;
} */

SN * SNavail(){

	int i;

	if (av%1000==0) {
	    snarray = ckalloc(1000*sizeof(SN) );
	    av = 0;
	}
	
	snarray[av].sbe = 0;
	snarray[av].sae = 0;
	snarray[av].ind = 0;
	snarray[av].next = NULL;

	av++;
	totalSN++;
        if(totalSN%100000==0) {
          if(debug>1) fprintf(stdout, "memory for SN: %d * %d\n", totalSN, sizeof(SN) );
        }

	return (&snarray[av-1]);

}
	   
	    
	

void AddSbe(SN *node, int indi, int s)
{
	SN *nd; 
	nd = node;
	if(!node) {
	  node = SNavail();
	  node->ind = indi;
	  node->sbe = s;
	  if(debug>1)fprintf(stdout, "%d ", sizeof(node) );
	  if(debug>1)fprintf(stdout, "%d %d\n", node->ind, node->sbe);
	  return;
	}
	else {
	  while(nd->next!=NULL) {
	     if(nd->ind == indi) {
	  	nd->sbe +=s;
		return;
	     }
	     nd = nd->next;
	  }
	  nd = SNavail();
	  nd->ind = indi;
	  nd->sbe = s;
	}
}

void printLib(int gi, int gj)
{
	int i,j,k,l;
	SN *nd;

	if( (gi>=ngroups) || (gj>=ngroups) ) {
	  fprintf(stdout, "group number exceeds the boundary: max(%d, %d) >= %d", gi, gj, ngroups);
	  exit(0);
	}

	fprintf(stdout, "Group %d and Group %d\n", gi, gj);
	for(i=1;i<=groupptr[gi]->seqlength;i++) {
	  nd = glib[gi][gj][i];
	  while(nd) {
	     fprintf(stdout, "%d %d %d %d\n", i, nd->ind, nd->sae, nd->sbe);
	     nd = nd->next;
	  }
	}
}  

void printMatrix(int gi, int gj)
{
	int i,j,k,l;
   	SN *nd;
	
	int **matrix;
	matrix = ckalloc( (groupptr[gi]->seqlength+1) *sizeof(int *) );
	for(j=1;j<=groupptr[gi]->seqlength;j++) {
	    matrix[j] = ckalloc( (groupptr[gj]->seqlength+1)*sizeof(int) );
	}

 	for(i=1;i<=groupptr[gi]->seqlength;i++) {
	   for(j=1;j<=groupptr[gj]->seqlength;j++) {
		matrix[i][j] = 0;
	   }
	}

  	for(i=1;i<=groupptr[gi]->seqlength;i++) {
	   nd = glib[gi][gj][i];
	   while(nd) {
	      matrix[i][nd->ind] = nd->sae;
	      nd = nd->next;
	   }
	}

	fprintf(stdout, "  ");
	for(j=1;j<=groupptr[gj]->seqlength;j++) {
	   fprintf(stdout, "%5c", am[groupptr[gj]->seq[1][j]]);
	}
	fprintf(stdout, "\n");

	for(i=1;i<=groupptr[gi]->seqlength;i++) {
	   fprintf(stdout, "%c ", am[groupptr[gi]->seq[1][i]]);
	   for(j=1;j<=groupptr[gj]->seqlength;j++) {
	   	fprintf(stdout, "%5d", matrix[i][j]);
	   }
	   fprintf(stdout, "\n");
	}

	for(j=1;j<=groupptr[gi]->seqlength;j++) ckfree((void *)matrix[j]);
	ckfree((void *)matrix);
}


void printAbstract(int set)
{
	int i,j,k,l,gid;

  	streeptr tr = grp_ancestor[set];
	
	for(i=1;i<=tr->abseqnum;i++) {
	   gid = tr->abstractseq[i][0];
	   for(k=1;k<=groupptr[gid]->seqnum;k++) {
	
	     fprintf(stdout, "%s\t", groupptr[gid]->name[k]);
	     for(j=1;j<=tr->abseqlength;j++) {
		if(tr->abstractseq[i][j]==0) {
		     fprintf(stdout, "-");
		}
		else {
		     fprintf(stdout, "%c", am[groupptr[gid]->seq[k][grp_ancestor[set]->abstractseq[i][j]]]);
		}
	      }
	      fprintf(stdout, "\n");
	   }
	 }
}
