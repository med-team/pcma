char **clusfilelist;
int *seqnumlist;
int filecount;
int *consensus_index;

void generate_subtree_roots() {

        int i, j, k;
        int startn, endn;  // starting and ending sequence number of the seq_array that specifying the subtree

        startn = 1;
        for(i=1;i<=filecount;i++) {
                endn = startn+seqnumlist[i]-1;
                calculate_tmat(startn, endn);
                guide_tree(tree, seqnumlist[i]);
                startn += seqnumlist[i];
        }
}

void calculate_tmat(int startnum, int endnum)
{
        int i,j,k,m,n;
        int identitycount, nongapcount;

        tmat = (double **) ckalloc( (startnum-endnum+2) * sizeof(double *) );
        for(i=1;i<=startnum-endnum+1;i++)
                tmat[i] = (double *) ckalloc( (startnum-endnum+1) * sizeof(double) );
        for(i=startnum;i<=endnum;i++) {
            for(j=i+1;j<=endnum;j++) {

                m = i-startnum+1;
                n = j-startnum+1;

                identitycount = nongapcount = 0;
                for(k=1;k<=seqlen_array[i];k++) {
                        if( (seq_array[i][k]==seq_array[j][k]) && (seq_array[i][k]!=gap_pos2) && (seq_array[j][k]!=gap_pos2) )
                        {
                                identitycount++;
                        }
                        if( (seq_array[i][k]!=gap_pos2) && (seq_array[j][k]!=gap_pos2) )
                        {
                                nongapcount++;
                        }
                }
                if(nongapcount>0) tmat[m][n] = tmat[n][m] = 1.0 - 1.0*identitycount/nongapcount;
                else {tmat[m][n] = 1; }
           }
        }
}
